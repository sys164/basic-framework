<!DOCTYPE html>
<html lang="en">
<head>

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>KingFish Club Site Installation</title>
  <meta name="copyright" content="Copyright Wray Services">
  <meta name="description" content="This is the installation script for KingFish Club Template">

  <meta name="robots" content="index, follow">
  <meta name="author" content="Chris Wray">
</head>
<body>
  <?php
  $club_settings = array();
  $colour_settings = array();
  $tmp_club = "<?php \n\n" ;
  $tmp_colour = "<?php \n" ;
  $colour_section = '';

  foreach($_POST as $key => $value){
    if(substr($key, 0, 4) == 'club') {
      $club_settings[$key] = $value;
      $tmp_club = $tmp_club."define('".strtoupper($key)."', '".ucwords(strtolower($value))."'); \n";
    } else {
      $value = '#'.str_replace('#', '', strtolower($value));
      if(substr($key, 0, strpos($key, '_')) != $colour_section) {
        $tmp_colour = $tmp_colour."\n";
        $colour_section = substr($key, 0, strpos($key, '_'));
      }
      $colour_settings[$key] = $value;
      $tmp_colour = $tmp_colour."define('".strtoupper($key)."', '".strtolower($value)."'); \n";
    }
  }
  $tmp_club = $tmp_club."\n?>";
  $tmp_colour = $tmp_colour."\n?>";

  $result_club = file_put_contents("../config/config.club.php", $tmp_club);
  $result_colour = file_put_contents("../config/config.colour.php", $tmp_colour);

  if(is_int($result_club) && is_int($result_colour)) {
    echo "Success!";
  } else {
    echo "Something went wrong";
  }
  ?>
</body>
</html>