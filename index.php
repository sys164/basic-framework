<?php
// Start the session
  session_start();

//  if(!file_exists('config.inc.php')) {
  if(!file_exists('config/config.colour.php') || !file_exists('config/config.club.php')) {
    header("Location: install/index.php");
  } else {
//    include_once('config.inc.php');
    include_once('config/config.club.php');
    include_once('config/config.colour.php');
#    include_once('functions.php');
  }

?>

<!DOCTYPE html>
<html lang="en">
<head>

  <title>Basic Framework</title>
  <meta name="DC.title" content="Basic Framework">
  <meta http-equiv="last-modified" content="Thur, 26 Dec 2019 23:30:00 GMT" />

  <?php include_once('favicon.php'); ?>

  <link rel="stylesheet" href="css/fonts.css" type="text/css" charset="utf-8" />
  <link rel="stylesheet" href="css/layout.php" type="text/css" charset="utf-8" />
<!---  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> --->
  <script src="menu.js"></script> 
</head>
<body onload="openPage('Tab1', this, '<?php echo $settings->paper_colour; ?>');"> 

<!--- openPage('Tab1', 'defaultOpen', <?php echo $settings->paper_colour ?>); --->
<!---  document.getElementById('defaultOpen').style.backgroundColor = '<?php echo $settings->paper_colour ?>'; --->

<div id="page">

  <div style="min-height: 200px; width: 100%; background-color: #ffffff; color: #000000; display: none;">
    <?php
    include('functions.php');

    $tabs = get_tabs();

    echo "<p><pre>";
    echo "Is this working?";
    print_r($tabs);
    echo "</pre></p>";
    ?>
  </div>

  <div id="header">
    <?php 
      include('pages/header.php'); 
    ?>

  <?php
    $tabs = get_tabs();
    $x = count($tabs);
  ?>
  <div id="menu">
  <?php
    for($i=1; $i<=$x-1; $i++) {
      echo "<button class=\"tablink\" onclick=\"openPage('Tab".$i."', this, '".PAPER_BG_COLOUR."')\">".$tabs[$i]['title']."</button>";
    }
    echo "<button class=\"tablink\" onclick=\"openPage('Tab".$x."', this, '".PAPER_BG_COLOUR."')\" style=\"border-right: none;\">".$tabs[$x]['title']."</button>";
  ?>
  </div>
  </div>

  <div id="content">
  <?php

    for($i=1; $i<=$x; $i++) {
      echo "<div id=\"Tab".$i."\" class=\"tabcontent\">";
      echo "<p>".$dir.$tabs[$i]['filename'].".php</p>";
//      echo "<p><pre>";
//      print_r($_SESSION);
//      echo "</pre></p>";
      echo "</div>";
    }

  ?>
  </div>

  <div id="footer">
    <?php include('pages/footer.php'); ?>
  </div>
</div>
</body>
</html>
