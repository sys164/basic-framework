<?php
function get_tabs() {
#  $dir = $_SERVER['HTTP_HOST']."/pages/";
  $dir = "./pages/";
  $a = scandir($dir);
  $tabs = "";
  $c = 0;
  $tabs = array();
  foreach ($a as $filename) {
    $id = "title";
    $b = stripos($filename, ".php");
    if($b > 0 && is_numeric(substr($filename, 0, $b))) {
      $c = $c+1;

      $handle = fopen($dir.$filename, 'r');
      $valid = false;
      $title = "";
      while (($buffer = fgets($handle)) !== false) {
        if (strpos($buffer, $id) !== false) {
          $valid = TRUE;
          $d = strpos($buffer, "=")+1;
          $title = substr($buffer, $d, strlen($buffer)-$d);
          $title = trim(str_replace(';', '', str_replace('"', '', $title)));
          break; // Once you find the string, you should break out the loop.
        }      
      }
      fclose($handle);

      $tabs[$c]['filename'] = substr($filename, 0, $b);
      if(strlen($title)>0) {
        $tabs[$c]['title'] = $title;
      } else {
        $tabs[$c]['title'] = "Tab ".$tabs[$c]['filename'];
      }
    }
  }
  return $tabs;
}
?>